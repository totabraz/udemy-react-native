import React, { Component } from "react";
import { Text, TouchableWithoutFeedback, View } from "react-native";
import { connect } from 'react-redux';
import {  CardSection } from "./common";
import * as actions from '../actions'

class ListItem extends Component {
    
    renderDescription() {
        const {library, expanded } = this.props;
        if(expanded) {
            return (
                <Text>
                    {library.item.description}
                </Text>
            );
        }
    }
    render() {
        const  { titleStyle, contetStyle } = styles; 
        const {id, title } = this.props.library.item;
        return(
            <TouchableWithoutFeedback
                onPress={() => this.props.selectLibrary(id)}>
                <View>
                    <CardSection style={contetStyle}>
                        <Text style={titleStyle}>
                            {title}
                        </Text>
                        <Text style={titleStyle}>
                            {this.renderDescription()}
                        </Text>
                    </CardSection>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles =  {
    contetStyle:{
        flexDirection: 'coloumn',
    },
    titleStyle:{
        flex:1,
        fontSize: 18,
        paddingLeft: 15,
    }
}

const mapStateToProps = (state, ownProps) => {
    const expanded = state.selectLibraryId === ownProps.library.item.id;
    return {  expanded }
}

// null is the first argument
export default connect(mapStateToProps, actions)(ListItem);